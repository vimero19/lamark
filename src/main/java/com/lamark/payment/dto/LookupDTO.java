package com.lamark.payment.dto;

/**
 * @author Rony Villanueva <rony.villanueva@avantica.net>
 * Created on 10/14/2018
 */
public class LookupDTO {

    private String scheme;

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @Override
    public String toString() {
        return "LookupDTO{" +
                "scheme='" + scheme + '\'' +
                '}';
    }
}