package com.lamark.payment.dto;

import java.math.BigDecimal;

/**
 * @author Rony Villanueva <rony.villanueva@avantica.net>
 * Created on 10/11/2018
 */
public class PaymentDTO {

    private String merchantId;
    private BigDecimal amount;
    private String pan;
    private String expirationDate;
    private String cvcCode;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCvcCode() {
        return cvcCode;
    }

    public void setCvcCode(String cvcCode) {
        this.cvcCode = cvcCode;
    }

}