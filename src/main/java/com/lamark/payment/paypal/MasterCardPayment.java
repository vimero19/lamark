package com.lamark.payment.paypal;

import com.lamark.payment.dto.PaymentDTO;
import com.lamark.payment.service.PaymentService;
import org.springframework.stereotype.Service;

/**
 * @author Rony Villanueva
 * Created on 10/11/2018
 */
@Service("mastercard")
public class MasterCardPayment implements PaymentService {

    @Override
    public void payment(PaymentDTO payment) {
        logger.info("Sending payment to MasterCard Provider: {}", payment);
    }

}