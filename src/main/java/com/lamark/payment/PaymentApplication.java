package com.lamark.payment;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rony Villanueva <rony.villanueva>
 * Created on 10/14/2018
 */
@SpringBootApplication
public class PaymentApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(PaymentApplication.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }

}