package com.lamark.payment.service;


import com.lamark.payment.dto.PaymentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface PaymentService {

    Logger logger = LoggerFactory.getLogger(PaymentService.class);

    void payment(PaymentDTO payment);

}