package com.lamark.payment.api.controller;

import com.lamark.payment.dto.PaymentDTO;
import com.lamark.payment.facade.PaypalFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

/**
 * @author Rony Villanueva <rony.villanueva>
 * Created on 10/14/2018
 */
@Controller
@RequestMapping("/v1/payment-management/payments")
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    private PaypalFacade facade;

    @Autowired
    public void setFacade(PaypalFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    public ResponseEntity<?> payments(@RequestBody PaymentDTO dto){
        try{
            logger.info("[POST] START /payments");
            facade.payment(dto);
            return ResponseEntity.ok(new HashMap<>());
        }catch(NoSuchBeanDefinitionException e){
            logger.error("Provider not supported");
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }finally {
            logger.info("[POST] END /payments");
        }
    }

}