package com.lamark.payment.component;

import com.lamark.payment.api.controller.PaymentController;
import com.lamark.payment.config.ExternalAPI;
import com.lamark.payment.dto.LookupDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author Rony Villanueva <rony.villanueva@avantica.net>
 * Created on 10/14/2018
 */
@Component
public class IntegrationComponent {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    private RestTemplate restTemplate;
    private ExternalAPI externalAPI;

    public IntegrationComponent(RestTemplate restTemplate, ExternalAPI externalAPI) {
        this.restTemplate = restTemplate;
        this.externalAPI = externalAPI;
    }

    public String getScheme(String number){
        String informationEndpoint  = externalAPI.getInformation(number);
        ResponseEntity<LookupDTO> responseEntity = restTemplate.getForEntity(informationEndpoint, LookupDTO.class, (Object) null);
        return responseEntity.getBody().getScheme();
    }

}