package com.lamark.payment.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Rony Villanueva <rony.villanueva>
 * Created on 10/14/2018
 */
@Configuration
public class FactoryConfig {

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ExternalAPI getExternalAPI() {
        return new ExternalAPI();
    }

}