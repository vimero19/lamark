package com.lamark.payment.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Rony Villanueva <rony.villanueva>
 * Created on 10/14/2018
 */
@ConfigurationProperties(prefix = "lamark.external.binlist")
public class ExternalAPI {

    private static final String SEPARATOR = "/";

    private String lookup;

    public String getLookup() {
        return lookup;
    }

    public void setLookup(String lookup) {
        this.lookup = lookup;
    }

    public String getInformation(String pan){
        StringBuilder endpoint = new StringBuilder(lookup);
        endpoint = endpoint.append(SEPARATOR).append(pan.substring(0,6));
        return endpoint.toString();
    }

}