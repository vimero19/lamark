package com.lamark.payment.facade;

import com.lamark.payment.component.IntegrationComponent;
import com.lamark.payment.dto.PaymentDTO;
import com.lamark.payment.service.PaymentService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Service;

/**
 * @author Rony Villanueva
 * Created on 10/11/2018
 */
@Service
public class PaypalFacade {

    private BeanFactory strategyFactory;

    private IntegrationComponent integrationComponent;

    public PaypalFacade(BeanFactory strategyFactory, IntegrationComponent integrationComponent) {
        this.strategyFactory = strategyFactory;
        this.integrationComponent = integrationComponent;
    }

    public void payment(PaymentDTO payment){
        String scheme =  integrationComponent.getScheme(payment.getPan());
        strategyFactory.getBean(scheme, PaymentService.class).payment(payment);
    }

}