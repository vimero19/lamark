package com.lamark.payment.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lamark.payment.dto.PaymentDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Rony Villanueva <rony.villanueva@avantica.net>
 * Created on 10/14/2018
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("staging")
@RunWith(SpringRunner.class)
public class PaymentAPITest {

    private static final String ENDPOINT = "/v1/payment-management/payments";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldProcessVisaSchemeAndSuccessfully() throws Exception{
        PaymentDTO request =  new PaymentDTO();
        request.setPan("4444333322221111");
        mockMvc.perform(post(ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(request))).andExpect(status().isOk());
    }

    @Test
    public void shouldProcessMasterCardSchemeAndSuccessfully() throws Exception{
        PaymentDTO request =  new PaymentDTO();
        request.setPan("5385873156347514");
        mockMvc.perform(post(ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(request))).andExpect(status().isOk());
    }

    @Test
    public void whenProviderNotSupported() throws Exception{
        PaymentDTO request =  new PaymentDTO();
        request.setPan("377892079521048");
        mockMvc.perform(post(ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(request))).andExpect(status().isUnprocessableEntity());
    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}